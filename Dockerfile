FROM node:latest


WORKDIR /app
#Copy json file to install the libraries
COPY package.json /app/
#Install the libraries npm config set strict-ssl false && npm config set registry http://registry.npmjs.org/ &&
RUN npm install -g nodemon  && npm install
#Copy the code to the work directory
COPY . /app/

EXPOSE 8083 3000 
