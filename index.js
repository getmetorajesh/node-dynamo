var express = require('express')
var app = express()
const context = require('./context');
var dynamoose = require('dynamoose');

if(process.env.NODE_ENV !=="production"){
    dynamoose.local();

dynamoose.AWS.config.update({
  accessKeyId: 'AKID',
  secretAccessKey: 'SECRET',
  region: 'us-east-1',
  endpoint: "http://localhost:8000"
});
} else {
    dynamoose.AWS.config.update({
        accessKeyId: '',
        secretAccessKey: '',
        region: 'us-east-1',
        endpoint: ""
    });
}
var Cat = dynamoose.model('Cat', { id: Number, name: String });

app.use(context.setup);

app.get('/', function(req, res) {
    console.log(context.current())
    res.send('Hello World!');   
    Cat.get()
        .then(function (badCat) {
            console.log('Never trust a smiling cat. - ' + badCat.name);
            res.send('Hello World!' + badCat.name)
    });
    
})

app.get('/create', function(req, res) {
    // Create a new cat object
    var garfield = new Cat({id: 666, name: 'Garfield'});

    // Save to DynamoDB
    garfield.save();
    res.send('Hello World!')

})

app.listen(3000, function() {
    console.log('Example app listening on port 3000!')
})

//make it work with connect and swagger

// logLevel, corID, namespace, datetime, messageLog, uri, headers,
// remote_host, remote_port, method, host,