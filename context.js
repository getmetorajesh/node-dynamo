'use strict';

const continuation = require('continuation-local-storage');
const cuid = require('cuid');

const appName = "sample"; //require('config').get('app_name');

class Context {

    static test(err, req, res, next) {
        next();
    }

    static setup(req, res, next) {
        console.log('req');
        const session = continuation.createNamespace(appName);
        session.run(() => {
            session.set('context', new Context(req));
            next();
        });
    }

    constructor(req) {
        this.path = req.path;
        this.corrId = req.headers['x-correlation-id'] || cuid();
        this.requestTime = Date.now();
    }

    static current() {
        return continuation.getNamespace(appName).get('context');
    }
}

module.exports = Context;